﻿Imports MySql.Data.MySqlClient
Imports System.Data.SqlClient

Public Class Connection
    ' Koneksi
    Dim koneksi As New MySqlConnection
    Dim stringkoneksi As String = "Server=" + db_server + ";User Id=" + db_user + ";Password=" + db_pass + ";Database=" + db_name + ""
    Dim perintahmysql As New MySqlCommand
    Dim perintahsql As New SqlCommand
    Sub Connect()
        Try
            If koneksi Is Nothing Or Not koneksi.State = ConnectionState.Open Then 'jika tidak ada koneksi atau koneksi mysql tidak dibuka
                koneksi = New MySqlConnection(stringkoneksi)
                'membuka koneksi mysql
                koneksi.Open()
            Else
                koneksi.Close()
                koneksi = New MySqlConnection(stringkoneksi)
                'membuka koneksi mysql
                koneksi.Open()
            End If
        Catch ex As Exception

            MsgBox("Koneksi Gagal: " & ex.ToString)
            Console.WriteLine("Koneksi gagal")
            Application.Exit()

        End Try
    End Sub
    Function Command(ByVal query As String)
        Connect()
        cmd = New MySqlCommand(query, koneksi)
        Return cmd
    End Function

    Function Execute(ByVal query As String)
        Connect()
        Try
            With perintahmysql
                .CommandText = query
                .CommandType = CommandType.Text
                .Connection = koneksi
                .ExecuteNonQuery()
            End With
            Return True
        Catch ex As Exception
            MsgBox(ex.ToString)
            Return False
        End Try
    End Function
End Class
