﻿Public Class LoginDosen
    Dim DB As Connection = New Connection()
    Dim backToMain As Boolean = True
    Dim user_id As String

    Private Sub btn_back_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_back.Click
        Main.Show()
        Close()
    End Sub

    Private Sub LoginDosen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        fillTujuan()
    End Sub

    Private Sub fillTujuan()
        DB.Connect()
        rd = DB.Command("select * from lb_tujuan").ExecuteReader
        Do While rd.Read()
            cbox_tujuan.Items.Add(rd.Item("tjn_kegiatan"))
            ',MsgBox(rd.Item("tjn_kegiatan"))
        Loop
    End Sub


    Private Sub btn_masuk_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_masuk.Click

        backToMain = False
        If tbox_nidn.Text <> "" Then

            Dim tjn_id, usr_id, kom_id As String
            Try
                auth_user = user_id
                usr_id = user_id
                DB.Connect()
                rd = DB.Command("select * from lb_tujuan where tjn_kegiatan = '" & cbox_tujuan.Text & "'").ExecuteReader
                rd.Read()
                If rd.HasRows Then
                    tjn_id = rd.Item("tjn_id")
                    rd.Close()
                    rd = DB.Command("select * from lb_dosen where dsn_nidn = '" & tbox_nidn.Text & "'").ExecuteReader
                    rd.Read()
                    If rd.HasRows Then
                        auth_data.Add("id", rd.Item("dsn_nidn"))
                        auth_data.Add("nama", rd.Item("dsn_nama"))
                    End If
                    rd.Close()

                    rd = DB.Command("select * from lb_komputer where kom_id = '" & My.Settings.pc_name & "'").ExecuteReader
                    rd.Read()
                    If rd.HasRows Then
                        kom_id = rd.Item("kom_id")
                        auth_data.Add("id_pc", rd.Item("kom_id"))
                        auth_data.Add("no_pc", rd.Item("kom_nama"))
                    End If
                    auth_data.Add("tujuan", cbox_tujuan.Text)
                    auth_data.Add("role", "dosen")

                    If DB.Execute("insert into lb_pengguna (`tjn_id`,`usr_id`,`kom_id`,`pgn_datetime`, `pgn_mulai`, `pgn_last_update`,`pgn_role`) values('" & tjn_id & "', '" & usr_id & "', '" & kom_id & "', now(), now(),now(),'dosen')") Then
                        rd = DB.Command("select * from lb_pengguna where tjn_id = '" & tjn_id & "' and usr_id = '" & usr_id & "' order by pgn_datetime desc limit 1").ExecuteReader
                        rd.Read()
                        If rd.HasRows Then
                            auth_data.Add("sess_id", rd.Item("pgn_id"))

                            Dim DKey As DisableKeyboardvb = New DisableKeyboardvb()
                            DKey.UnhookKeyboard()
                            ' Start Explorer.exe
                            Dim ExProcess = New Process()
                            ExProcess.StartInfo.UseShellExecute = True
                            ExProcess.StartInfo.CreateNoWindow = True
                            ExProcess.StartInfo.FileName = "c:\windows\explorer.exe"
                            ExProcess.StartInfo.WorkingDirectory = Application.StartupPath
                            ExProcess.Start()

                            'Stop Timer Kill
                            MainContainer.Timer1.Stop()

                            Status.Show()
                            MainContainer.Close()
                            Close()
                        Else
                            MsgBox("Gagal mengumpulkan informasi :(", MsgBoxStyle.Critical)
                        End If
                    End If
                Else
                    MsgBox("Tujuan Pengunaan tidak valid :(", MsgBoxStyle.Exclamation)
                End If
            Catch ex As Exception
                MsgBox("Terjadi Kesalahan :( (" & ex.ToString & ")", MsgBoxStyle.Critical)
            End Try
        Else
            MsgBox("Masukan data yang diminta!", MsgBoxStyle.Exclamation)
        End If

    End Sub

    Private Sub tbox_nidn_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbox_nidn.KeyPress
        If Not Char.IsDigit(e.KeyChar) And Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub tbox_nidn_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbox_nidn.Validated
        If tbox_nidn.Text <> "" Then
            '' Check jika user terdaftar
            DB.Connect()

            rd = DB.Command("select * from lb_dosen where dsn_nidn = '" & tbox_nidn.Text & "'").ExecuteReader
            rd.Read()
            If rd.HasRows Then
                'MsgBox(rd.Item("dsn_nama"))
                tbox_nama.Text = rd.Item("dsn_nama")
                user_id = rd.Item("dsn_nidn")
                tbox_nama.ReadOnly = True
                btn_masuk.Enabled = True
            Else
                MsgBox("Anda tidak terdaftar :(", MsgBoxStyle.Exclamation)
                btn_masuk.Enabled = False
                tbox_nama.ReadOnly = False
            End If
        End If
    End Sub

    Private Sub tbox_nidn_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbox_nidn.TextChanged

    End Sub

    Private Sub LoginDosen_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LocationChanged
        Dim r As Rectangle
        If Parent IsNot Nothing Then
            r = Parent.RectangleToScreen(Parent.ClientRectangle)
        Else
            r = Screen.FromPoint(Location).WorkingArea
        End If

        Dim x = r.Left + (r.Width - Width) \ 2
        Dim y = r.Top + (r.Height - Height) \ 2
        Location = New Point(x, y)
    End Sub
End Class