﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LoginMahasiswa
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_back = New System.Windows.Forms.PictureBox()
        Me.btn_masuk = New System.Windows.Forms.Button()
        Me.cbox_tujuan = New System.Windows.Forms.ComboBox()
        Me.tbox_nama = New System.Windows.Forms.TextBox()
        Me.tbox_nim = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Logo = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        CType(Me.btn_back, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btn_back)
        Me.Panel1.Controls.Add(Me.btn_masuk)
        Me.Panel1.Controls.Add(Me.cbox_tujuan)
        Me.Panel1.Controls.Add(Me.tbox_nama)
        Me.Panel1.Controls.Add(Me.tbox_nim)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Logo)
        Me.Panel1.Location = New System.Drawing.Point(13, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(359, 442)
        Me.Panel1.TabIndex = 1
        '
        'btn_back
        '
        Me.btn_back.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_back.BackgroundImage = Global.LogBook_Laboratorium_Pemrograman.My.Resources.Resources.back_button_icon_2510
        Me.btn_back.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn_back.Location = New System.Drawing.Point(-1, 394)
        Me.btn_back.Name = "btn_back"
        Me.btn_back.Size = New System.Drawing.Size(50, 50)
        Me.btn_back.TabIndex = 8
        Me.btn_back.TabStop = False
        '
        'btn_masuk
        '
        Me.btn_masuk.Location = New System.Drawing.Point(236, 324)
        Me.btn_masuk.Name = "btn_masuk"
        Me.btn_masuk.Size = New System.Drawing.Size(75, 23)
        Me.btn_masuk.TabIndex = 7
        Me.btn_masuk.Text = "MASUK"
        Me.btn_masuk.UseVisualStyleBackColor = True
        '
        'cbox_tujuan
        '
        Me.cbox_tujuan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbox_tujuan.FormattingEnabled = True
        Me.cbox_tujuan.Location = New System.Drawing.Point(144, 293)
        Me.cbox_tujuan.Name = "cbox_tujuan"
        Me.cbox_tujuan.Size = New System.Drawing.Size(167, 21)
        Me.cbox_tujuan.TabIndex = 6
        '
        'tbox_nama
        '
        Me.tbox_nama.Location = New System.Drawing.Point(144, 264)
        Me.tbox_nama.Name = "tbox_nama"
        Me.tbox_nama.Size = New System.Drawing.Size(167, 20)
        Me.tbox_nama.TabIndex = 5
        Me.tbox_nama.Visible = False
        '
        'tbox_nim
        '
        Me.tbox_nim.Location = New System.Drawing.Point(144, 232)
        Me.tbox_nim.Name = "tbox_nim"
        Me.tbox_nim.Size = New System.Drawing.Size(167, 20)
        Me.tbox_nim.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(42, 293)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Tujuan Pengunaan"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(42, 264)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Nama"
        Me.Label2.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(42, 232)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "NIM"
        '
        'Logo
        '
        Me.Logo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Logo.Image = Global.LogBook_Laboratorium_Pemrograman.My.Resources.Resources.tole_crop
        Me.Logo.Location = New System.Drawing.Point(95, 19)
        Me.Logo.Name = "Logo"
        Me.Logo.Size = New System.Drawing.Size(174, 186)
        Me.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Logo.TabIndex = 0
        Me.Logo.TabStop = False
        '
        'LoginMahasiswa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 461)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.Name = "LoginMahasiswa"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login Mahasiswa"
        Me.TopMost = True
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.btn_back, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Logo As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbox_tujuan As System.Windows.Forms.ComboBox
    Friend WithEvents tbox_nama As System.Windows.Forms.TextBox
    Friend WithEvents tbox_nim As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btn_masuk As System.Windows.Forms.Button
    Friend WithEvents btn_back As System.Windows.Forms.PictureBox
End Class
