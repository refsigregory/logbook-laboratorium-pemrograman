﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_next = New System.Windows.Forms.Button()
        Me.GroupBoxPengguna = New System.Windows.Forms.GroupBox()
        Me.radio_tamu = New System.Windows.Forms.RadioButton()
        Me.radio_dosen = New System.Windows.Forms.RadioButton()
        Me.radio_mahasiswa = New System.Windows.Forms.RadioButton()
        Me.Logo = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        Me.GroupBoxPengguna.SuspendLayout()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btn_next)
        Me.Panel1.Controls.Add(Me.GroupBoxPengguna)
        Me.Panel1.Controls.Add(Me.Logo)
        Me.Panel1.Location = New System.Drawing.Point(13, 13)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(359, 436)
        Me.Panel1.TabIndex = 0
        '
        'btn_next
        '
        Me.btn_next.Location = New System.Drawing.Point(127, 318)
        Me.btn_next.Name = "btn_next"
        Me.btn_next.Size = New System.Drawing.Size(97, 23)
        Me.btn_next.TabIndex = 2
        Me.btn_next.Text = "SELANJUTNYA"
        Me.btn_next.UseVisualStyleBackColor = True
        '
        'GroupBoxPengguna
        '
        Me.GroupBoxPengguna.Controls.Add(Me.radio_tamu)
        Me.GroupBoxPengguna.Controls.Add(Me.radio_dosen)
        Me.GroupBoxPengguna.Controls.Add(Me.radio_mahasiswa)
        Me.GroupBoxPengguna.Location = New System.Drawing.Point(95, 212)
        Me.GroupBoxPengguna.Name = "GroupBoxPengguna"
        Me.GroupBoxPengguna.Size = New System.Drawing.Size(200, 100)
        Me.GroupBoxPengguna.TabIndex = 1
        Me.GroupBoxPengguna.TabStop = False
        Me.GroupBoxPengguna.Text = "Pengguna"
        '
        'radio_tamu
        '
        Me.radio_tamu.AutoSize = True
        Me.radio_tamu.Location = New System.Drawing.Point(51, 65)
        Me.radio_tamu.Name = "radio_tamu"
        Me.radio_tamu.Size = New System.Drawing.Size(52, 17)
        Me.radio_tamu.TabIndex = 2
        Me.radio_tamu.TabStop = True
        Me.radio_tamu.Text = "Tamu"
        Me.radio_tamu.UseVisualStyleBackColor = True
        '
        'radio_dosen
        '
        Me.radio_dosen.AutoSize = True
        Me.radio_dosen.Location = New System.Drawing.Point(51, 42)
        Me.radio_dosen.Name = "radio_dosen"
        Me.radio_dosen.Size = New System.Drawing.Size(56, 17)
        Me.radio_dosen.TabIndex = 1
        Me.radio_dosen.TabStop = True
        Me.radio_dosen.Text = "Dosen"
        Me.radio_dosen.UseVisualStyleBackColor = True
        '
        'radio_mahasiswa
        '
        Me.radio_mahasiswa.AutoSize = True
        Me.radio_mahasiswa.Location = New System.Drawing.Point(51, 19)
        Me.radio_mahasiswa.Name = "radio_mahasiswa"
        Me.radio_mahasiswa.Size = New System.Drawing.Size(78, 17)
        Me.radio_mahasiswa.TabIndex = 0
        Me.radio_mahasiswa.TabStop = True
        Me.radio_mahasiswa.Text = "Mahasiswa"
        Me.radio_mahasiswa.UseVisualStyleBackColor = True
        '
        'Logo
        '
        Me.Logo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Logo.Image = Global.LogBook_Laboratorium_Pemrograman.My.Resources.Resources.tole_crop
        Me.Logo.Location = New System.Drawing.Point(95, 19)
        Me.Logo.Name = "Logo"
        Me.Logo.Size = New System.Drawing.Size(174, 186)
        Me.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Logo.TabIndex = 0
        Me.Logo.TabStop = False
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 392)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Main"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Logbook Laboratorium Pemrograman"
        Me.TopMost = True
        Me.Panel1.ResumeLayout(False)
        Me.GroupBoxPengguna.ResumeLayout(False)
        Me.GroupBoxPengguna.PerformLayout()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Logo As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBoxPengguna As System.Windows.Forms.GroupBox
    Friend WithEvents radio_tamu As System.Windows.Forms.RadioButton
    Friend WithEvents radio_dosen As System.Windows.Forms.RadioButton
    Friend WithEvents radio_mahasiswa As System.Windows.Forms.RadioButton
    Friend WithEvents btn_next As System.Windows.Forms.Button

End Class
