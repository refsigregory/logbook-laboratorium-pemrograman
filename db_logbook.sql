-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2018 at 02:18 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_logbook`
--

-- --------------------------------------------------------

--
-- Table structure for table `lb_dosen`
--

CREATE TABLE `lb_dosen` (
  `dsn_nidn` int(10) NOT NULL,
  `dsn_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lb_dosen`
--

INSERT INTO `lb_dosen` (`dsn_nidn`, `dsn_nama`) VALUES
(1, 'UTU SIHOMBING'),
(2, 'TEST'),
(3, 'TEST 2');

-- --------------------------------------------------------

--
-- Table structure for table `lb_fakultas`
--

CREATE TABLE `lb_fakultas` (
  `fkt_id` int(2) NOT NULL,
  `fkt_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lb_fakultas`
--

INSERT INTO `lb_fakultas` (`fkt_id`, `fkt_nama`) VALUES
(1, 'TEKNIK');

-- --------------------------------------------------------

--
-- Table structure for table `lb_komputer`
--

CREATE TABLE `lb_komputer` (
  `kom_id` int(2) NOT NULL,
  `kom_nama` varchar(25) NOT NULL,
  `kom_ip` varchar(25) NOT NULL,
  `kom_mac` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lb_komputer`
--

INSERT INTO `lb_komputer` (`kom_id`, `kom_nama`, `kom_ip`, `kom_mac`) VALUES
(0, 'Komputer Test', '127.0.0.1', '00:00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `lb_mahasiswa`
--

CREATE TABLE `lb_mahasiswa` (
  `mhs_nim` int(8) NOT NULL,
  `prd_id` int(2) NOT NULL,
  `mhs_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lb_mahasiswa`
--

INSERT INTO `lb_mahasiswa` (`mhs_nim`, `prd_id`, `mhs_nama`) VALUES
(1001, 1, 'ALO SIHOMBING'),
(1002, 1, 'Test'),
(1003, 1, 'Test  2'),
(1004, 1, 'Test 3'),
(1006, 0, 'Test 5'),
(1007, 1, 'Test 7');

-- --------------------------------------------------------

--
-- Table structure for table `lb_pengguna`
--

CREATE TABLE `lb_pengguna` (
  `pgn_id` int(11) NOT NULL,
  `tjn_id` int(11) NOT NULL,
  `kom_id` int(2) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `pgn_datetime` datetime NOT NULL,
  `pgn_mulai` datetime NOT NULL,
  `pgn_selesai` datetime NOT NULL,
  `pgn_last_update` datetime NOT NULL,
  `pgn_role` enum('mahasiswa','dosen','tamu') NOT NULL,
  `pgn_riwayat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lb_pengguna`
--

INSERT INTO `lb_pengguna` (`pgn_id`, `tjn_id`, `kom_id`, `usr_id`, `pgn_datetime`, `pgn_mulai`, `pgn_selesai`, `pgn_last_update`, `pgn_role`, `pgn_riwayat`) VALUES
(1, 1, 0, 1001, '2018-10-27 00:00:00', '2018-10-27 00:00:00', '2018-10-27 00:00:00', '2018-10-27 00:00:00', 'mahasiswa', ''),
(3, 2, 0, 1001, '2018-10-27 18:58:33', '2018-10-27 18:58:33', '2018-10-27 18:58:33', '2018-10-27 18:58:33', 'mahasiswa', ''),
(4, 1, 0, 1001, '2018-10-27 18:58:47', '2018-10-27 18:58:47', '2018-10-27 18:58:47', '2018-10-27 18:58:47', 'mahasiswa', ''),
(5, 3, 0, 1001, '2018-10-29 22:35:08', '2018-10-29 22:35:08', '2018-10-29 22:35:08', '2018-10-29 22:35:08', 'mahasiswa', ''),
(6, 1, 0, 1, '2018-10-29 22:41:54', '2018-10-29 22:41:54', '2018-10-29 22:41:54', '2018-10-29 22:41:54', 'dosen', ''),
(7, 3, 0, 1, '2018-10-29 22:45:07', '2018-10-29 22:45:07', '2018-10-29 22:45:07', '2018-10-29 22:45:07', 'tamu', ''),
(8, 1, 0, 1, '2018-11-05 22:07:48', '2018-11-05 22:07:48', '2018-11-05 22:07:48', '2018-11-05 22:07:48', 'tamu', ''),
(9, 2, 0, 1, '2018-11-05 22:23:53', '2018-11-05 22:23:53', '2018-11-05 22:23:53', '2018-11-05 22:23:53', 'tamu', ''),
(10, 2, 0, 1001, '2018-11-05 22:36:44', '2018-11-05 22:36:44', '2018-11-05 22:36:44', '2018-11-05 22:36:44', 'mahasiswa', ''),
(11, 2, 0, 1001, '2018-11-05 22:53:17', '2018-11-05 22:53:17', '2018-11-05 22:53:17', '2018-11-05 22:53:17', 'mahasiswa', ''),
(12, 2, 0, 1001, '2018-11-05 22:55:42', '2018-11-05 22:55:42', '2018-11-05 22:55:42', '2018-11-05 22:55:42', 'mahasiswa', ''),
(13, 1, 0, 1001, '2018-11-05 23:01:52', '2018-11-05 23:01:52', '2018-11-05 23:01:52', '2018-11-05 23:01:52', 'mahasiswa', ''),
(14, 1, 0, 1001, '2018-11-05 23:03:22', '2018-11-05 23:03:22', '2018-11-05 23:03:22', '2018-11-05 23:03:22', 'mahasiswa', ''),
(15, 3, 0, 1001, '2018-11-05 23:03:34', '2018-11-05 23:03:34', '2018-11-05 23:03:34', '2018-11-05 23:03:34', 'mahasiswa', ''),
(18, 1, 0, 1002, '2018-11-12 06:32:06', '2018-11-12 06:32:06', '2018-11-12 06:32:53', '2018-11-12 06:32:51', 'mahasiswa', ''),
(19, 1, 0, 1003, '2018-11-12 06:41:12', '2018-11-12 06:41:12', '2018-11-12 06:42:20', '2018-11-12 06:42:17', 'mahasiswa', ''),
(20, 2, 0, 2, '2018-11-12 06:43:48', '2018-11-12 06:43:48', '2018-11-12 06:43:54', '2018-11-12 06:43:53', 'dosen', ''),
(21, 2, 0, 1001, '2018-11-12 06:46:51', '2018-11-12 06:46:51', '2018-11-12 06:46:59', '2018-11-12 06:46:56', 'mahasiswa', ''),
(22, 1, 0, 1001, '2018-11-12 06:47:42', '2018-11-12 06:47:42', '2018-11-12 06:47:44', '2018-11-12 06:47:42', 'mahasiswa', 'LogonUI\r\n\r\nsvchost\r\n\r\nLogBook Laboratorium Pemrograman.vshost\r\n\r\nIntelliTrace\r\n\r\nsihost\r\n\r\nfirefox\r\n\r\nconhost\r\n\r\nFakeClient\r\n\r\nApplicationFrameHost\r\n\r\ndwm\r\n\r\ntaskhostw\r\n\r\nsvchost\r\n\r\nlsass\r\n\r\nsvchost\r\n\r\ndllhost\r\n\r\nservices\r\n\r\nexplorer\r\n\r\ncsrss\r\n\r\nfirefox\r\n\r\nsvchost\r\n\r\nSearchUI\r\n\r\nsvchost\r\n\r\nsvchost\r\n\r\nfirefox\r\n\r\nmysqld\r\n\r\nhttpd\r\n\r\ntaskhostw\r\n\r\ncsrss\r\n\r\ncmd\r\n\r\nhttpd\r\n\r\nsvchost\r\n\r\nspoolsv\r\n\r\nwlms\r\n\r\nRuntimeBroker\r\n\r\nsvchost\r\n\r\nWmiPrvSE\r\n\r\nwinlogon\r\n\r\npowershell\r\n\r\nexplorer\r\n\r\nnetsh\r\n\r\nfirefox\r\n\r\nbash\r\n\r\nsvchost\r\n\r\nsvchost\r\n\r\ndevenv\r\n\r\ncmd\r\n\r\nfirefox\r\n\r\nsvchost\r\n\r\nsmss\r\n\r\ndwm\r\n\r\nconhost\r\n\r\nconhost\r\n\r\nAppVShNotify\r\n\r\nOfficeClickToRun\r\n\r\nrdpclip\r\n\r\nfirefox\r\n\r\nsplwow64\r\n\r\nfirefox\r\n\r\nsvchost\r\n\r\nmsdtc\r\n\r\ngit-bash\r\n\r\nwinlogon\r\n\r\nSystemSettings\r\n\r\nsvchost\r\n\r\ncsrss\r\n\r\nmintty\r\n\r\nAAct_x64\r\n\r\nwininit\r\n\r\nsvchost\r\n\r\nsvchost\r\n\r\nsvchost\r\n\r\nAppVShNotify\r\n\r\nconhost\r\n\r\nShellExperienceHost\r\n\r\nSystem\r\n\r\nsvchost\r\n\r\nIdle\r\n\r\n'),
(23, 1, 0, 1, '2018-11-12 06:52:26', '2018-11-12 06:52:26', '2018-11-12 06:52:28', '2018-11-12 06:52:26', 'tamu', 'LogonUI\r\n\r\nsvchost\r\n\r\nLogBook Laboratorium Pemrograman.vshost\r\n\r\nIntelliTrace\r\n\r\nsihost\r\n\r\nfirefox\r\n\r\nconhost\r\n\r\nFakeClient\r\n\r\nApplicationFrameHost\r\n\r\ndwm\r\n\r\ntaskhostw\r\n\r\nsvchost\r\n\r\nlsass\r\n\r\nsvchost\r\n\r\ndllhost\r\n\r\nservices\r\n\r\nexplorer\r\n\r\ncsrss\r\n\r\nfirefox\r\n\r\nsvchost\r\n\r\nSearchUI\r\n\r\nsvchost\r\n\r\nsvchost\r\n\r\nfirefox\r\n\r\nmysqld\r\n\r\nhttpd\r\n\r\ntaskhostw\r\n\r\ncsrss\r\n\r\ncmd\r\n\r\nhttpd\r\n\r\nsvchost\r\n\r\nspoolsv\r\n\r\nwlms\r\n\r\nRuntimeBroker\r\n\r\nsvchost\r\n\r\nWmiPrvSE\r\n\r\nwinlogon\r\n\r\npowershell\r\n\r\nexplorer\r\n\r\nnetsh\r\n\r\nfirefox\r\n\r\nbash\r\n\r\nsvchost\r\n\r\nsvchost\r\n\r\ndevenv\r\n\r\ncmd\r\n\r\nfirefox\r\n\r\nsvchost\r\n\r\nsmss\r\n\r\ndwm\r\n\r\nconhost\r\n\r\nconhost\r\n\r\nAppVShNotify\r\n\r\nOfficeClickToRun\r\n\r\nrdpclip\r\n\r\nfirefox\r\n\r\nsplwow64\r\n\r\nfirefox\r\n\r\nsvchost\r\n\r\nmsdtc\r\n\r\ngit-bash\r\n\r\nwinlogon\r\n\r\nSystemSettings\r\n\r\nsvchost\r\n\r\ncsrss\r\n\r\nmintty\r\n\r\nAAct_x64\r\n\r\nwininit\r\n\r\nsvchost\r\n\r\nsvchost\r\n\r\nsvchost\r\n\r\nAppVShNotify\r\n\r\nconhost\r\n\r\nShellExperienceHost\r\n\r\nSystem\r\n\r\nsvchost\r\n\r\nIdle\r\n\r\n'),
(24, 2, 0, 1004, '2018-11-12 06:54:55', '2018-11-12 06:54:55', '2018-11-12 06:55:00', '2018-11-12 06:54:55', 'mahasiswa', 'LogonUI\r\n\r\nsvchost\r\n\r\nLogBook Laboratorium Pemrograman.vshost\r\n\r\nIntelliTrace\r\n\r\nsihost\r\n\r\nfirefox\r\n\r\nconhost\r\n\r\nFakeClient\r\n\r\nApplicationFrameHost\r\n\r\ndwm\r\n\r\ntaskhostw\r\n\r\nsvchost\r\n\r\nlsass\r\n\r\nsvchost\r\n\r\ndllhost\r\n\r\nservices\r\n\r\nexplorer\r\n\r\ncsrss\r\n\r\nfirefox\r\n\r\nsvchost\r\n\r\nSearchUI\r\n\r\nsvchost\r\n\r\nsvchost\r\n\r\nfirefox\r\n\r\nmysqld\r\n\r\nhttpd\r\n\r\ntaskhostw\r\n\r\ncsrss\r\n\r\ncmd\r\n\r\nhttpd\r\n\r\nsvchost\r\n\r\nspoolsv\r\n\r\nwlms\r\n\r\nRuntimeBroker\r\n\r\nsvchost\r\n\r\nWmiPrvSE\r\n\r\nwinlogon\r\n\r\npowershell\r\n\r\nexplorer\r\n\r\nnetsh\r\n\r\nfirefox\r\n\r\nbash\r\n\r\nsvchost\r\n\r\nsvchost\r\n\r\ndevenv\r\n\r\ncmd\r\n\r\nfirefox\r\n\r\nsvchost\r\n\r\nsmss\r\n\r\ndwm\r\n\r\nconhost\r\n\r\nconhost\r\n\r\nAppVShNotify\r\n\r\nOfficeClickToRun\r\n\r\nrdpclip\r\n\r\nfirefox\r\n\r\nsplwow64\r\n\r\nfirefox\r\n\r\nsvchost\r\n\r\nmsdtc\r\n\r\ngit-bash\r\n\r\nwinlogon\r\n\r\nSystemSettings\r\n\r\nsvchost\r\n\r\ncsrss\r\n\r\nmintty\r\n\r\nAAct_x64\r\n\r\nwininit\r\n\r\nsvchost\r\n\r\nsvchost\r\n\r\nsvchost\r\n\r\nAppVShNotify\r\n\r\nconhost\r\n\r\nShellExperienceHost\r\n\r\nSystem\r\n\r\nsvchost\r\n\r\nIdle\r\n\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `lb_prodi`
--

CREATE TABLE `lb_prodi` (
  `prd_id` int(2) NOT NULL,
  `fkt_id` int(2) NOT NULL,
  `prd_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lb_prodi`
--

INSERT INTO `lb_prodi` (`prd_id`, `fkt_id`, `prd_name`) VALUES
(1, 1, 'INFORMATIKA');

-- --------------------------------------------------------

--
-- Table structure for table `lb_tamu`
--

CREATE TABLE `lb_tamu` (
  `tmu_code` varchar(20) NOT NULL,
  `tmu_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lb_tamu`
--

INSERT INTO `lb_tamu` (`tmu_code`, `tmu_nama`) VALUES
('1', 'MINCE SIHOMBING'),
('2', 'TEST'),
('3', 'TEST');

-- --------------------------------------------------------

--
-- Table structure for table `lb_tujuan`
--

CREATE TABLE `lb_tujuan` (
  `tjn_id` int(11) NOT NULL,
  `tjn_kegiatan` varchar(100) NOT NULL,
  `tjn_tahun_ajaran` varchar(10) NOT NULL DEFAULT '20001',
  `tjn_penanggung_jawab` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lb_tujuan`
--

INSERT INTO `lb_tujuan` (`tjn_id`, `tjn_kegiatan`, `tjn_tahun_ajaran`, `tjn_penanggung_jawab`) VALUES
(1, 'TESTING', '20181', 'TEST'),
(2, 'TESTING 2', '20181', ''),
(3, 'TESTING 3', '20181', 'TEST');

-- --------------------------------------------------------

--
-- Table structure for table `lb_users`
--

CREATE TABLE `lb_users` (
  `usr_id` int(11) NOT NULL,
  `usr_username` varchar(50) NOT NULL,
  `usr_password` varchar(50) NOT NULL,
  `usr_status` enum('koordinator','laboran','asslab') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lb_users`
--

INSERT INTO `lb_users` (`usr_id`, `usr_username`, `usr_password`, `usr_status`) VALUES
(1, 'admin', 'admin', 'koordinator'),
(2, 'alo', 'alo', 'asslab');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lb_dosen`
--
ALTER TABLE `lb_dosen`
  ADD PRIMARY KEY (`dsn_nidn`);

--
-- Indexes for table `lb_fakultas`
--
ALTER TABLE `lb_fakultas`
  ADD PRIMARY KEY (`fkt_id`);

--
-- Indexes for table `lb_komputer`
--
ALTER TABLE `lb_komputer`
  ADD PRIMARY KEY (`kom_id`);

--
-- Indexes for table `lb_mahasiswa`
--
ALTER TABLE `lb_mahasiswa`
  ADD PRIMARY KEY (`mhs_nim`);

--
-- Indexes for table `lb_pengguna`
--
ALTER TABLE `lb_pengguna`
  ADD PRIMARY KEY (`pgn_id`),
  ADD KEY `lst_id` (`tjn_id`,`kom_id`,`usr_id`);

--
-- Indexes for table `lb_prodi`
--
ALTER TABLE `lb_prodi`
  ADD PRIMARY KEY (`prd_id`),
  ADD KEY `fkt_id` (`fkt_id`);

--
-- Indexes for table `lb_tamu`
--
ALTER TABLE `lb_tamu`
  ADD PRIMARY KEY (`tmu_code`);

--
-- Indexes for table `lb_tujuan`
--
ALTER TABLE `lb_tujuan`
  ADD PRIMARY KEY (`tjn_id`);

--
-- Indexes for table `lb_users`
--
ALTER TABLE `lb_users`
  ADD PRIMARY KEY (`usr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lb_fakultas`
--
ALTER TABLE `lb_fakultas`
  MODIFY `fkt_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lb_komputer`
--
ALTER TABLE `lb_komputer`
  MODIFY `kom_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lb_pengguna`
--
ALTER TABLE `lb_pengguna`
  MODIFY `pgn_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `lb_prodi`
--
ALTER TABLE `lb_prodi`
  MODIFY `prd_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lb_tujuan`
--
ALTER TABLE `lb_tujuan`
  MODIFY `tjn_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lb_users`
--
ALTER TABLE `lb_users`
  MODIFY `usr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lb_pengguna`
--
ALTER TABLE `lb_pengguna`
  ADD CONSTRAINT `lb_pengguna_ibfk_1` FOREIGN KEY (`tjn_id`) REFERENCES `lb_tujuan` (`tjn_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
