﻿Public Class Main
    Dim DB As New Connection()

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ShowList()
        If auth_data("role") <> "koordinator" Then
            LaporanToolStripMenuItem.Visible = False
            PenggunaToolStripMenuItem.Visible = False
        Else
            LaporanToolStripMenuItem.Visible = True
            PenggunaToolStripMenuItem.Visible = True
        End If

    End Sub

    Sub ShowList(Optional ByVal query As String = "")
        Try
            DB.Connect()
            If query <> "" Then
                cmd = DB.Command("select pengguna.pgn_role as role, pengguna.usr_id, pengguna.pgn_mulai as mulai, pengguna.pgn_selesai as selesai, pengguna.pgn_datetime as tanggal, pengguna.pgn_riwayat as riwayat, komputer.kom_nama as no_pc, tujuan.tjn_kegiatan as tujuan,tujuan.tjn_tahun_ajaran as tahun, (case pengguna.pgn_role when 'mahasiswa' then (select mhs_nama from lb_mahasiswa where mhs_nim = pengguna.usr_id) when 'dosen' then (select dsn_nama from lb_dosen where dsn_nidn = pengguna.usr_id) when 'tamu' then (select tmu_nama from lb_tamu where tmu_code = pengguna.usr_id) end) as nama from  lb_pengguna pengguna join lb_komputer komputer on komputer.kom_id = pengguna.kom_id join lb_tujuan tujuan on tujuan.tjn_id = pengguna .tjn_id " &
                                 "where pengguna.pgn_selesai != '0000-00-00 00:00:00' and komputer.kom_nama like '%" & query & "%' OR (case pengguna.pgn_role when 'mahasiswa' then (select mhs_nama from lb_mahasiswa where mhs_nim = pengguna.usr_id) when 'dosen' then (select dsn_nama from lb_dosen where dsn_nidn = pengguna.usr_id) when 'tamu' then (select tmu_nama from lb_tamu where tmu_code = pengguna.usr_id) end) like '%" & query & "%'  OR pengguna.usr_id like '%" & query & "%' OR tujuan.tjn_kegiatan like '%" & query & "%' OR tujuan.tjn_tahun_ajaran like '%" & query & "%' OR pengguna.pgn_riwayat like '%" & query & "%' order by pengguna.pgn_datetime desc")
            Else
                cmd = DB.Command("select pengguna.pgn_role as role, pengguna.usr_id, pengguna.pgn_mulai as mulai, pengguna.pgn_selesai as selesai, pengguna.pgn_datetime as tanggal, pengguna.pgn_riwayat as riwayat, komputer.kom_nama as no_pc, tujuan.tjn_kegiatan as tujuan,tujuan.tjn_tahun_ajaran as tahun, (case pengguna.pgn_role when 'mahasiswa' then (select mhs_nama from lb_mahasiswa where mhs_nim = pengguna.usr_id) when 'dosen' then (select dsn_nama from lb_dosen where dsn_nidn = pengguna.usr_id) when 'tamu' then (select tmu_nama from lb_tamu where tmu_code = pengguna.usr_id) end) as nama from  lb_pengguna pengguna join lb_komputer komputer on komputer.kom_id = pengguna.kom_id join lb_tujuan tujuan on tujuan.tjn_id = pengguna .tjn_id where pengguna.pgn_selesai != '0000-00-00 00:00:00'  order by pengguna.pgn_datetime desc")
            End If
            rd = cmd.ExecuteReader
            'Membersihkan Semua Baris di DataGridView
            data_pengguna.Rows.Clear()
            Do While rd.Read
                Dim tanggal As String = Format(rd.Item("tanggal"), "dd-MM-yyyy")
                Dim id_usr As String = rd.Item("usr_id")
                Dim nama As String = rd.Item("nama")
                Dim no_pc As String = rd.Item("no_pc")
                Dim tujuan As String = rd.Item("tujuan")
                Dim mulai As String
                Dim selesai As String

                If rd.Item("mulai").ToString <> "0000-00-00 00:00:00" Then
                    If Format(rd.Item("mulai"), "HH:mm:ss").ToString <> "00:00:00" Then
                        mulai = Format(rd.Item("mulai"), "HH:mm:ss")
                    Else
                        mulai = "-"
                    End If
                Else
                    mulai = "-"
                End If

                    If rd.Item("selesai").ToString <> "0000-00-00 00:00:00" Then
                    If Format(rd.Item("selesai"), "HH:mm:ss").ToString <> "00:00:00" Then
                        selesai = Format(rd.Item("selesai"), "HH:mm:ss")
                    Else
                        selesai = "-"
                    End If
                    Else
                        selesai = "-"
                    End If

                    Dim tahun As String = rd.Item("tahun")
                    Dim role As String = rd.Item("role")
                    Dim riwayat As String = rd.Item("riwayat")

                    'Menambahkan baris baru pada DataGridView
                    data_pengguna.Rows.Add(tanggal, no_pc, id_usr, nama, tujuan, mulai, selesai, tahun, riwayat)
            Loop
        Catch ex As Exception
            MsgBox("Terjadi kesalahan :( (" & ex.ToString & ")", MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub LihatToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LihatToolStripMenuItem.Click
        'Main.Show()
        'Close()
    End Sub

    Private Sub LaporanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanToolStripMenuItem.Click
        Laporan.Show()
        Close()
    End Sub

    Private Sub AdminToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AkunAdmin.Show()
        Close()
    End Sub


    Private Sub AdminToolStripMenuItem_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AdminToolStripMenuItem.Click
        AkunAdmin.Show()
        Close()
    End Sub

    Private Sub MahasiswaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MahasiswaToolStripMenuItem.Click
        AkunMahasiswa.Show()
        Close()
    End Sub

    Private Sub DosenToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DosenToolStripMenuItem.Click
        AkunDosen.Show()
        Close()
    End Sub

    Private Sub TamuToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TamuToolStripMenuItem.Click
        AkunTamu.Show()
        Close()
    End Sub

    Private Sub TujuanPenggunaanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TujuanPenggunaanToolStripMenuItem.Click
        PengaturanTujuanPenggunaan.Show()
        Close()
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KeluarToolStripMenuItem.Click
        Dim answer As DialogResult
        answer = MessageBox.Show("Apakah Anda Yakin Ingin Keluar?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If answer = vbYes Then
            auth_data.Clear()
            auth_user = ""
            Login.Show()
            Close()
        End If
    End Sub

    Private Sub tbox_pencarian_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbox_pencarian.TextChanged
        ShowList(tbox_pencarian.Text)
    End Sub

End Class