﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PengaturanTujuanPenggunaan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.tbox_pencarian = New System.Windows.Forms.TextBox()
        Me.btn_hapus = New System.Windows.Forms.Button()
        Me.btn_ubah = New System.Windows.Forms.Button()
        Me.btn_tambah = New System.Windows.Forms.Button()
        Me.data_tujuan = New System.Windows.Forms.DataGridView()
        Me.No = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Kegiatan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PenangungJawab = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cbox_tahun_ajaran = New System.Windows.Forms.NumericUpDown()
        Me.MenuList = New System.Windows.Forms.MenuStrip()
        Me.LihatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengaturanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TujuanPenggunaanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AkunToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PenggunaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MahasiswaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DosenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TamuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KeluarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1.SuspendLayout()
        CType(Me.data_tujuan, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbox_tahun_ajaran, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuList.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.tbox_pencarian)
        Me.GroupBox1.Controls.Add(Me.btn_hapus)
        Me.GroupBox1.Controls.Add(Me.btn_ubah)
        Me.GroupBox1.Controls.Add(Me.btn_tambah)
        Me.GroupBox1.Controls.Add(Me.data_tujuan)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 64)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(960, 385)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tujuan Penggunaan"
        '
        'tbox_pencarian
        '
        Me.tbox_pencarian.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbox_pencarian.Location = New System.Drawing.Point(625, 19)
        Me.tbox_pencarian.Name = "tbox_pencarian"
        Me.tbox_pencarian.Size = New System.Drawing.Size(182, 20)
        Me.tbox_pencarian.TabIndex = 6
        '
        'btn_hapus
        '
        Me.btn_hapus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_hapus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus.Location = New System.Drawing.Point(823, 207)
        Me.btn_hapus.Name = "btn_hapus"
        Me.btn_hapus.Size = New System.Drawing.Size(122, 46)
        Me.btn_hapus.TabIndex = 5
        Me.btn_hapus.Text = "Hapus"
        Me.btn_hapus.UseVisualStyleBackColor = True
        '
        'btn_ubah
        '
        Me.btn_ubah.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_ubah.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ubah.Location = New System.Drawing.Point(823, 155)
        Me.btn_ubah.Name = "btn_ubah"
        Me.btn_ubah.Size = New System.Drawing.Size(122, 46)
        Me.btn_ubah.TabIndex = 4
        Me.btn_ubah.Text = "Ubah"
        Me.btn_ubah.UseVisualStyleBackColor = True
        '
        'btn_tambah
        '
        Me.btn_tambah.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_tambah.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah.Location = New System.Drawing.Point(823, 103)
        Me.btn_tambah.Name = "btn_tambah"
        Me.btn_tambah.Size = New System.Drawing.Size(122, 46)
        Me.btn_tambah.TabIndex = 2
        Me.btn_tambah.Text = "Tambah"
        Me.btn_tambah.UseVisualStyleBackColor = True
        '
        'data_tujuan
        '
        Me.data_tujuan.AllowUserToAddRows = False
        Me.data_tujuan.AllowUserToDeleteRows = False
        Me.data_tujuan.AllowUserToOrderColumns = True
        Me.data_tujuan.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.data_tujuan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.data_tujuan.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.data_tujuan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.data_tujuan.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.No, Me.Kegiatan, Me.PenangungJawab})
        Me.data_tujuan.Location = New System.Drawing.Point(7, 61)
        Me.data_tujuan.Name = "data_tujuan"
        Me.data_tujuan.ReadOnly = True
        Me.data_tujuan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.data_tujuan.Size = New System.Drawing.Size(800, 289)
        Me.data_tujuan.TabIndex = 1
        '
        'No
        '
        Me.No.FillWeight = 20.0!
        Me.No.HeaderText = "No"
        Me.No.Name = "No"
        Me.No.ReadOnly = True
        '
        'Kegiatan
        '
        Me.Kegiatan.FillWeight = 90.74619!
        Me.Kegiatan.HeaderText = "Kegiatan"
        Me.Kegiatan.Name = "Kegiatan"
        Me.Kegiatan.ReadOnly = True
        '
        'PenangungJawab
        '
        Me.PenangungJawab.FillWeight = 75.0!
        Me.PenangungJawab.HeaderText = "Penangung Jawab"
        Me.PenangungJawab.Name = "PenangungJawab"
        Me.PenangungJawab.ReadOnly = True
        '
        'cbox_tahun_ajaran
        '
        Me.cbox_tahun_ajaran.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbox_tahun_ajaran.Location = New System.Drawing.Point(852, 38)
        Me.cbox_tahun_ajaran.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.cbox_tahun_ajaran.Minimum = New Decimal(New Integer() {20001, 0, 0, 0})
        Me.cbox_tahun_ajaran.Name = "cbox_tahun_ajaran"
        Me.cbox_tahun_ajaran.Size = New System.Drawing.Size(120, 20)
        Me.cbox_tahun_ajaran.TabIndex = 3
        Me.cbox_tahun_ajaran.Value = New Decimal(New Integer() {20181, 0, 0, 0})
        '
        'MenuList
        '
        Me.MenuList.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LihatToolStripMenuItem, Me.LaporanToolStripMenuItem, Me.PengaturanToolStripMenuItem, Me.KeluarToolStripMenuItem})
        Me.MenuList.Location = New System.Drawing.Point(0, 0)
        Me.MenuList.Name = "MenuList"
        Me.MenuList.Size = New System.Drawing.Size(984, 24)
        Me.MenuList.TabIndex = 4
        Me.MenuList.Text = "Menu"
        '
        'LihatToolStripMenuItem
        '
        Me.LihatToolStripMenuItem.Name = "LihatToolStripMenuItem"
        Me.LihatToolStripMenuItem.Size = New System.Drawing.Size(129, 20)
        Me.LihatToolStripMenuItem.Text = "Lihat Data Pengguna"
        '
        'LaporanToolStripMenuItem
        '
        Me.LaporanToolStripMenuItem.Name = "LaporanToolStripMenuItem"
        Me.LaporanToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.LaporanToolStripMenuItem.Text = "Laporan"
        '
        'PengaturanToolStripMenuItem
        '
        Me.PengaturanToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TujuanPenggunaanToolStripMenuItem, Me.AkunToolStripMenuItem})
        Me.PengaturanToolStripMenuItem.Name = "PengaturanToolStripMenuItem"
        Me.PengaturanToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.PengaturanToolStripMenuItem.Text = "Pengaturan"
        '
        'TujuanPenggunaanToolStripMenuItem
        '
        Me.TujuanPenggunaanToolStripMenuItem.Name = "TujuanPenggunaanToolStripMenuItem"
        Me.TujuanPenggunaanToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.TujuanPenggunaanToolStripMenuItem.Text = "Tujuan Penggunaan"
        '
        'AkunToolStripMenuItem
        '
        Me.AkunToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AdminToolStripMenuItem, Me.PenggunaToolStripMenuItem})
        Me.AkunToolStripMenuItem.Name = "AkunToolStripMenuItem"
        Me.AkunToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.AkunToolStripMenuItem.Text = "Akun"
        '
        'AdminToolStripMenuItem
        '
        Me.AdminToolStripMenuItem.Name = "AdminToolStripMenuItem"
        Me.AdminToolStripMenuItem.Size = New System.Drawing.Size(128, 22)
        Me.AdminToolStripMenuItem.Text = "Admin"
        '
        'PenggunaToolStripMenuItem
        '
        Me.PenggunaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MahasiswaToolStripMenuItem, Me.DosenToolStripMenuItem, Me.TamuToolStripMenuItem})
        Me.PenggunaToolStripMenuItem.Name = "PenggunaToolStripMenuItem"
        Me.PenggunaToolStripMenuItem.Size = New System.Drawing.Size(128, 22)
        Me.PenggunaToolStripMenuItem.Text = "Pengguna"
        '
        'MahasiswaToolStripMenuItem
        '
        Me.MahasiswaToolStripMenuItem.Name = "MahasiswaToolStripMenuItem"
        Me.MahasiswaToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.MahasiswaToolStripMenuItem.Text = "Mahasiswa"
        '
        'DosenToolStripMenuItem
        '
        Me.DosenToolStripMenuItem.Name = "DosenToolStripMenuItem"
        Me.DosenToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.DosenToolStripMenuItem.Text = "Dosen"
        '
        'TamuToolStripMenuItem
        '
        Me.TamuToolStripMenuItem.Name = "TamuToolStripMenuItem"
        Me.TamuToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.TamuToolStripMenuItem.Text = "Tamu"
        '
        'KeluarToolStripMenuItem
        '
        Me.KeluarToolStripMenuItem.Name = "KeluarToolStripMenuItem"
        Me.KeluarToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.KeluarToolStripMenuItem.Text = "Keluar"
        '
        'PengaturanTujuanPenggunaan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 461)
        Me.Controls.Add(Me.MenuList)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cbox_tahun_ajaran)
        Me.Name = "PengaturanTujuanPenggunaan"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pengaturan"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.data_tujuan, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbox_tahun_ajaran, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuList.ResumeLayout(False)
        Me.MenuList.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents data_tujuan As System.Windows.Forms.DataGridView
    Friend WithEvents btn_tambah As System.Windows.Forms.Button
    Friend WithEvents btn_hapus As System.Windows.Forms.Button
    Friend WithEvents btn_ubah As System.Windows.Forms.Button
    Friend WithEvents cbox_tahun_ajaran As System.Windows.Forms.NumericUpDown
    Friend WithEvents tbox_pencarian As System.Windows.Forms.TextBox
    Friend WithEvents No As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Kegiatan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PenangungJawab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MenuList As System.Windows.Forms.MenuStrip
    Friend WithEvents LihatToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengaturanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TujuanPenggunaanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AkunToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdminToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PenggunaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MahasiswaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DosenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TamuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KeluarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
