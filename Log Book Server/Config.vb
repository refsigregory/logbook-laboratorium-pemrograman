﻿Imports MySql.Data.MySqlClient

Module Config
    Public db_server As String = My.Settings.db_server
    Public db_user As String = My.Settings.db_user
    Public db_pass As String = My.Settings.db_pass
    Public db_name As String = My.Settings.db_name
    Public auth_user As String = ""
    Public auth_data As New Dictionary(Of String, String) From {}
    Public da As MySqlDataAdapter
    Public dt As DataTable
    Public ds As DataSet
    Public cd As MySqlCommand
    Public cmd As MySqlCommand
    Public rd, dr As MySqlDataReader
    Public time As String = Format(Now, "dd/MM/yyyy HH:mm:ss")
End Module
