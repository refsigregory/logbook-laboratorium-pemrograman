﻿Imports System.Windows.Forms

Public Class DialogAkunMahasiswa
    Public idUsr, idPrd, idFkt, selPrd, prdName, fktName As String
    Public mode As String = "add"
    Dim DB As New Connection()
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        DB.Connect()
        rd = DB.Command("select * from lb_fakultas join lb_prodi on lb_prodi.fkt_id = lb_fakultas.fkt_id where lb_fakultas.fkt_nama= '" & cbox_fakultas.Text & "' ").ExecuteReader
        rd.Read()
        If rd.HasRows Then
            idPrd = rd.Item("prd_id")

        If mode = "add" Then
            If DB.Execute("insert into lb_mahasiswa (`mhs_nim`,`mhs_nama`,`prd_id`) values('" & tbox_nim.Text & "', '" & tbox_nama.Text & "', '" & idPrd & "')") Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                    MsgBox("Data Berhasil Disimpan!", vbInformation)
                AkunMahasiswa.Close()
                AkunMahasiswa.Show()

                Me.Close()
            End If
        ElseIf mode = "edit" Then
            If DB.Execute("update lb_mahasiswa set `mhs_nim`='" & tbox_nim.Text & "',`prd_id`='" & idPrd & "',`mhs_nama`='" & tbox_nama.Text & "' where mhs_nim= '" & idUsr & "'") Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                    MsgBox("Data Berhasil Diubah!", vbInformation)
                AkunMahasiswa.Close()
                AkunMahasiswa.Show()

                Me.Close()
            End If
            End If
        Else
            MsgBox("Gagal mendapatkan ID Fakultas", MsgBoxStyle.Critical)
        End If
        rd.Close()

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub DialogAkunMahasiswa_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        AkunMahasiswa.btn_tambah.Enabled = True
        AkunMahasiswa.btn_ubah.Enabled = True
        AkunMahasiswa.btn_hapus.Enabled = True
        AkunMahasiswa.data_users.Enabled = True
    End Sub

    Private Sub DialogTujuanPenggunaan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If mode = "edit" Then
            Dim prd_id, fkt_id As String
            Try
                DB.Connect()
                rd = DB.Command("select * from lb_mahasiswa where mhs_nim= '" & idUsr & "' ").ExecuteReader
                rd.Read()
                If rd.HasRows Then
                    tbox_nim.Text = rd.Item("mhs_nim")
                    tbox_nama.Text = rd.Item("mhs_nama")

                    rd.Close()
                    rd = DB.Command("select * from lb_prodi where prd_name= '" & prdName & "' ").ExecuteReader
                    rd.Read()
                    If rd.HasRows Then
                        prd_id = rd.Item("prd_id")
                        cbox_prodi.Text = rd.Item("prd_name")
                        fkt_id = rd.Item("fkt_id")
                        rd.Close()
                        rd = DB.Command("select * from lb_fakultas where fkt_nama= '" & fktName & "' ").ExecuteReader
                        rd.Read()
                        If rd.HasRows Then
                            cbox_fakultas.Text = rd.Item("fkt_nama")
                        End If
                    End If

                Else
                    MsgBox("Data tidak ditemukan :(", MsgBoxStyle.Exclamation)
                End If
            Catch ex As Exception
                MsgBox("Terjadi Kesalahan :( (" & ex.ToString & ")", MsgBoxStyle.Critical)
            End Try
        End If

    End Sub

    Private Sub tbox_nim_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbox_nim.KeyPress
        If Not Char.IsDigit(e.KeyChar) And Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub tbox_nim_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbox_nim.TextChanged

    End Sub

    Private Sub DialogAkunMahasiswa_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LocationChanged
        Dim r As Rectangle
        If Parent IsNot Nothing Then
            r = Parent.RectangleToScreen(Parent.ClientRectangle)
        Else
            r = Screen.FromPoint(Location).WorkingArea
        End If

        Dim x = r.Left + (r.Width - Width) \ 2
        Dim y = r.Top + (r.Height - Height) \ 2
        Location = New Point(x, y)
    End Sub
End Class
