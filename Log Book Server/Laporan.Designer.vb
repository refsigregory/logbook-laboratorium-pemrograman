﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Laporan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.db_logbookDataSet = New Log_Book_Server.db_logbookDataSet()
        Me.lb_penggunaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lb_penggunaTableAdapter = New Log_Book_Server.db_logbookDataSetTableAdapters.lb_penggunaTableAdapter()
        Me.MySqlMonitor1 = New Devart.Data.MySql.MySqlMonitor()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.LogBook4 = New Log_Book_Server.LogBook()
        Me.LogBook2 = New Log_Book_Server.LogBook()
        Me.LogBook1 = New Log_Book_Server.LogBook()
        Me.LogBook3 = New Log_Book_Server.LogBook()
        Me.cbox_tahun_ajaran = New System.Windows.Forms.NumericUpDown()
        Me.MenuList = New System.Windows.Forms.MenuStrip()
        Me.LihatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengaturanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TujuanPenggunaanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AkunToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PenggunaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MahasiswaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DosenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TamuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KeluarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.db_logbookDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lb_penggunaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cbox_tahun_ajaran, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuList.SuspendLayout()
        Me.SuspendLayout()
        '
        'db_logbookDataSet
        '
        Me.db_logbookDataSet.DataSetName = "db_logbookDataSet"
        Me.db_logbookDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lb_penggunaBindingSource
        '
        Me.lb_penggunaBindingSource.DataMember = "lb_pengguna"
        Me.lb_penggunaBindingSource.DataSource = Me.db_logbookDataSet
        '
        'lb_penggunaTableAdapter
        '
        Me.lb_penggunaTableAdapter.ClearBeforeFill = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.CrystalReportViewer1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 41)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(960, 408)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Laporan"
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = 0
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(3, 16)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Me.LogBook4
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(954, 389)
        Me.CrystalReportViewer1.TabIndex = 0
        '
        'cbox_tahun_ajaran
        '
        Me.cbox_tahun_ajaran.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbox_tahun_ajaran.Location = New System.Drawing.Point(846, 27)
        Me.cbox_tahun_ajaran.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.cbox_tahun_ajaran.Minimum = New Decimal(New Integer() {20001, 0, 0, 0})
        Me.cbox_tahun_ajaran.Name = "cbox_tahun_ajaran"
        Me.cbox_tahun_ajaran.Size = New System.Drawing.Size(120, 20)
        Me.cbox_tahun_ajaran.TabIndex = 4
        Me.cbox_tahun_ajaran.Value = New Decimal(New Integer() {20181, 0, 0, 0})
        '
        'MenuList
        '
        Me.MenuList.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LihatToolStripMenuItem, Me.LaporanToolStripMenuItem, Me.PengaturanToolStripMenuItem, Me.KeluarToolStripMenuItem})
        Me.MenuList.Location = New System.Drawing.Point(0, 0)
        Me.MenuList.Name = "MenuList"
        Me.MenuList.Size = New System.Drawing.Size(984, 24)
        Me.MenuList.TabIndex = 5
        Me.MenuList.Text = "Menu"
        '
        'LihatToolStripMenuItem
        '
        Me.LihatToolStripMenuItem.Name = "LihatToolStripMenuItem"
        Me.LihatToolStripMenuItem.Size = New System.Drawing.Size(129, 20)
        Me.LihatToolStripMenuItem.Text = "Lihat Data Pengguna"
        '
        'LaporanToolStripMenuItem
        '
        Me.LaporanToolStripMenuItem.Name = "LaporanToolStripMenuItem"
        Me.LaporanToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.LaporanToolStripMenuItem.Text = "Laporan"
        '
        'PengaturanToolStripMenuItem
        '
        Me.PengaturanToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TujuanPenggunaanToolStripMenuItem, Me.AkunToolStripMenuItem})
        Me.PengaturanToolStripMenuItem.Name = "PengaturanToolStripMenuItem"
        Me.PengaturanToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.PengaturanToolStripMenuItem.Text = "Pengaturan"
        '
        'TujuanPenggunaanToolStripMenuItem
        '
        Me.TujuanPenggunaanToolStripMenuItem.Name = "TujuanPenggunaanToolStripMenuItem"
        Me.TujuanPenggunaanToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.TujuanPenggunaanToolStripMenuItem.Text = "Tujuan Penggunaan"
        '
        'AkunToolStripMenuItem
        '
        Me.AkunToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AdminToolStripMenuItem, Me.PenggunaToolStripMenuItem})
        Me.AkunToolStripMenuItem.Name = "AkunToolStripMenuItem"
        Me.AkunToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.AkunToolStripMenuItem.Text = "Akun"
        '
        'AdminToolStripMenuItem
        '
        Me.AdminToolStripMenuItem.Name = "AdminToolStripMenuItem"
        Me.AdminToolStripMenuItem.Size = New System.Drawing.Size(128, 22)
        Me.AdminToolStripMenuItem.Text = "Admin"
        '
        'PenggunaToolStripMenuItem
        '
        Me.PenggunaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MahasiswaToolStripMenuItem, Me.DosenToolStripMenuItem, Me.TamuToolStripMenuItem})
        Me.PenggunaToolStripMenuItem.Name = "PenggunaToolStripMenuItem"
        Me.PenggunaToolStripMenuItem.Size = New System.Drawing.Size(128, 22)
        Me.PenggunaToolStripMenuItem.Text = "Pengguna"
        '
        'MahasiswaToolStripMenuItem
        '
        Me.MahasiswaToolStripMenuItem.Name = "MahasiswaToolStripMenuItem"
        Me.MahasiswaToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.MahasiswaToolStripMenuItem.Text = "Mahasiswa"
        '
        'DosenToolStripMenuItem
        '
        Me.DosenToolStripMenuItem.Name = "DosenToolStripMenuItem"
        Me.DosenToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.DosenToolStripMenuItem.Text = "Dosen"
        '
        'TamuToolStripMenuItem
        '
        Me.TamuToolStripMenuItem.Name = "TamuToolStripMenuItem"
        Me.TamuToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.TamuToolStripMenuItem.Text = "Tamu"
        '
        'KeluarToolStripMenuItem
        '
        Me.KeluarToolStripMenuItem.Name = "KeluarToolStripMenuItem"
        Me.KeluarToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.KeluarToolStripMenuItem.Text = "Keluar"
        '
        'Laporan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 461)
        Me.Controls.Add(Me.MenuList)
        Me.Controls.Add(Me.cbox_tahun_ajaran)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Laporan"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Laporan"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.db_logbookDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lb_penggunaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.cbox_tahun_ajaran, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuList.ResumeLayout(False)
        Me.MenuList.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lb_penggunaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents db_logbookDataSet As Log_Book_Server.db_logbookDataSet
    Friend WithEvents lb_penggunaTableAdapter As Log_Book_Server.db_logbookDataSetTableAdapters.lb_penggunaTableAdapter
    Friend WithEvents MySqlMonitor1 As Devart.Data.MySql.MySqlMonitor
    Friend WithEvents LogBook1 As Log_Book_Server.LogBook
    Friend WithEvents LogBook2 As Log_Book_Server.LogBook
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents LogBook3 As Log_Book_Server.LogBook
    Friend WithEvents LogBook4 As Log_Book_Server.LogBook
    Friend WithEvents cbox_tahun_ajaran As System.Windows.Forms.NumericUpDown
    Friend WithEvents MenuList As System.Windows.Forms.MenuStrip
    Friend WithEvents LihatToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengaturanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TujuanPenggunaanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AkunToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdminToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PenggunaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MahasiswaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DosenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TamuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KeluarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
