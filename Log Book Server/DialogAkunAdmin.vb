﻿Imports System.Windows.Forms

Public Class DialogAkunAdmin
    Public idUsr As String
    Public mode As String = "add"
    Dim DB As New Connection()
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        If mode = "add" Then
            If DB.Execute("insert into lb_users (`usr_username`,`usr_password`,`usr_status`) values('" & tbox_username.Text & "', '" & tbox_password.Text & "', '" & cbox_role.Text & "')") Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK

                MsgBox("Data Berhasil Disimpan!", vbInformation)

                AkunAdmin.Close()
                AkunAdmin.Show()

                Me.Close()
            End If
        ElseIf mode = "edit" Then
            If DB.Execute("update lb_users set `usr_username`='" & tbox_username.Text & "',`usr_status`='" & cbox_role.Text & "',`usr_password`='" & tbox_password.Text & "' where usr_id = '" & idUsr & "'") Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                MsgBox("Data Berhasil Diubah!", vbInformation)
                AkunAdmin.Close()
                AkunAdmin.Show()

                Me.Close()
            End If
        End If

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub DialogAkunAdmin_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        AkunAdmin.btn_tambah.Enabled = True
        AkunAdmin.btn_ubah.Enabled = True
        AkunAdmin.btn_hapus.Enabled = True
        AkunAdmin.data_users.Enabled = True
    End Sub

    Private Sub DialogTujuanPenggunaan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If mode = "edit" Then
            Try
                DB.Connect()
                rd = DB.Command("select * from lb_users where usr_id = '" & idUsr & "' ").ExecuteReader
                rd.Read()
                If rd.HasRows Then
                    tbox_username.Text = rd.Item("usr_username")
                    tbox_password.Text = rd.Item("usr_password")
                    cbox_role.Text = rd.Item("usr_status")
                Else
                    MsgBox("Data tidak ditemukan :(", MsgBoxStyle.Exclamation)
                End If
            Catch ex As Exception
                MsgBox("Terjadi Kesalahan :( (" & ex.ToString & ")", MsgBoxStyle.Critical)
            End Try
        End If
    End Sub

    Private Sub DialogAkunAdmin_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LocationChanged
        Dim r As Rectangle
        If Parent IsNot Nothing Then
            r = Parent.RectangleToScreen(Parent.ClientRectangle)
        Else
            r = Screen.FromPoint(Location).WorkingArea
        End If

        Dim x = r.Left + (r.Width - Width) \ 2
        Dim y = r.Top + (r.Height - Height) \ 2
        Location = New Point(x, y)
    End Sub
End Class
